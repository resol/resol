![banner](assets/resol-banner.jpg)

<h1 align="center">reŠOL</h3>

<p align="center">
    Svobodná alternativní aplikace pro Školu Online
    <br />
    <a href="https://codeberg.org/resol/resol/wiki"><strong>Prohlédnout dokumentaci »</strong></a>
    <br />
    <br />
    <a href="https://re-sol.tech">Oficiální instance</a>
    ·
    <a href="https://codeberg.org/resol/resol/issues">Nahlásit chybu</a>
    ·
    <a href="https://codeberg.org/resol/resol/issues">Požádat o funkci</a>
</p>

*Tento projekt již není udržovaný ani hostovaný. Pokud vás zajímá jak funguje ŠOL API, můžete se podívat na naší [dokumentaci](/resol/resol/wiki/Home).*

<h1>🏠 O projektu</h1>

Tento projekt jsme začali kvůli neustálé nespokojenosti se Školou OnLine. Jejich web je zastaralý a neresponzivní. Nová aplikace sice napravila problém se vzhledem, ale stále jí chybí funkce jako tmavý režim a přinesla některé nové problémy.

Založili jsme proto tento projekt, ve kterém se jako studenti snažíme vylepšit zkušenost dalších studentů. Náš cíl je vytvořit modernější, soukromou a svobodnou verzi ŠOLu. Používáme aktuální vývojové praktiky a optimalizujeme UX pro mobily. Nikdy ale pravděpodobně nedosáhneme plné podpory, třeba kvůli jejich omezené API pro mobilní aplikace, kterou využíváme. Taky jsou limitující naše testovací možnosti, protože jako třetí strana můžeme testovat pouze věci, které se v ŠOLu objeví nám. `reŠOL` je v relativně rané fázi vývoje, momentálně nedokáže plně nahradit aplikaci Školy OnLine.

Vítáme každou pomoc, ať už s programováním či třeba grafikou.
Taky budeme rádi za jakoukoli zpětnou vazbu!

*Upozornění: `reŠOL` není jakýmkoli způsobem ovlivněn či spojen se společností BAKALÁŘI software s.r.o.*

<h1>🤔 Jak to funguje?</h1>

U frontendu se řídíme pravidly:
- žádné špehování ([re-sol.tech](https://re-sol.tech) ale používá Cloudflare)
- svižné a non-bloated stránky

Ve frontendu děláme requesty na `reŠOL` API, která odesílá requesty na Školu Online. Ta pak odesílá zpátky JSON soubor s daty, která parsujeme na stránku.
S tím se pojí problém blokování requestů kvůli rate limitingu (ŠOL API má určitý počet requestů, které může uživatel udělat za určitý čas a jakmile tento limit překročí, tak je začne blokovat).
  
<h1>🌟 Funkce</h1>

Máme funkční přihlašování na (snad) všechny ŠOL servery. V headeru se zobrazuje jméno a role (Jan Novák, student). Základní funkce rozvrhu fungují, jako zobrazování předmětů, učeben a akcí. Také lze rozkliknout jednotlivé předměty a zobrazit info o nich. V dashboardu najdeš funkční zprávy, hodnocení, domácí úkoly, akce a chování. Zprávy, známky a domácí úkoly lze rozklikávat pro zobrazení podrobností.

<h1>💻 Použití</h1>

`reŠOL` můžeš self-hostit, nebo použít naše instance:

- [re-sol.tech](https://re-sol.tech) (oficiální clear web stránka)
- [tor](http://resol7tch4znzhsftb5vjunk7d2pzsguqvlx75cw5rda3ami66tdrfad.onion/) (tor stránka)

Nejvíce testování probíhá na Gecku (Firefox) a na Blinku (Chrome). Snažíme se také o WebKit (Safari), ale zaměřujeme se primárně na předešlé dva web enginy.

<h1>🧑‍💻 Vývoj</h1>

Více informací najdeš na naší [wiki](/resol/resol/wiki/Home).

<h1>🪪 Licence</h1>

[![GPLv3](https://www.gnu.org/graphics/gplv3-with-text-136x68.png)](https://www.gnu.org/licenses/gpl-3.0)

`reŠOL` je svobodný software licencovaný pod [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html), což znamená, že máte
- Svobodu spustit program za jakýmkoliv účelem.
- Svobodu studovat, jak program pracuje a přizpůsobit ho svým potřebám.
- Svobodu redistribuovat kopie, abyste pomohli ostatním.
- Svobodu vylepšovat program a zveřejňovat zlepšení, aby z nich mohla mít prospěch celá komunita.

Tento program je distribuován s vírou, že bude užitečný, ale BEZ JAKÉKOLI ZÁRUKY; dokonce bez implicitní záruky PRODEJNÍ ZPŮSOBILOSTI nebo VHODNOSTI PRO KONKRÉTNÍ ÚČEL. Podívejte se na GNU General Public License pro více podrobností.

<h1>🤝 Poděkování</h1>

- https://bestofphp.com/repo/crpmax-skolaonline-app-api (stará API)

<h1>🖥️ Ukázky</h1>

![rozvrh-light](assets/resol-rozvrh.png)
![rozvrh-dark](assets/resol-rozvrh-dark.png)
