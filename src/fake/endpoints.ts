import { Application, Response } from "express"
import { getTokenData } from "./data/token"
import { getUserInfo } from "./data/user"
import { getBehaviorData } from "./data/behavior"
import { getTimetable } from "./data/timetable"

export function loadAPIEndpointsFakeV1(app: Application) {
  app.post('/solapi/api/connect/token', (_, res: Response) => {
    getTokenData(res)  
  })
  app.get('/solapi/api/v1/user', (_, res: Response) => {
    getUserInfo(res)
  })
  app.get('/solapi/api/v1/students/:id/behaviors', (_, res: Response) => {
    getBehaviorData(res)
  }) 
  app.get('/solapi/api/v1/timeTable', (_, res: Response) => {
    getTimetable(res)
  })
}
