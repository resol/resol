import express, { Request, Response } from 'express';
import cors from "cors";
import path from "node:path";
import winston from "winston";
import expressWinston from "express-winston";
import { Routes, getWebContent } from "./respond"
import { loadAPIEndpointsV1 } from "./api/v1/requests"
import { watchConfig } from './configWatcher';
import { checkConfig, configureServer, parseConfig } from './config';
import { loadAPIEndpointsFakeV1 } from './fake/endpoints';
import { loadSignalHandling } from './signals';

export const app = express()
export let server: any;

export const State = {
  config: {} as any,
  instanceName: "reŠOL",
  instanceDescription: "reŠOL server",
  serverHost: "127.0.0.1" as any,
  serverPort: 3009 as number
}

const transportFile = new winston.transports.File({
  filename: path.join(__dirname + '/../app.log'),
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.uncolorize(),
    winston.format.simple(),
    winston.format.json(),
  ),
})

export const logger = expressWinston.logger({
  transports: [
    new winston.transports.Console(),
    transportFile,
  ],
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.cli()
  )
})

export const infoLogger = winston.createLogger({
  transports: [
    new winston.transports.Console(),
    transportFile,
  ],
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.cli(),
  )
})

app.use(logger);
app.use(cors({
    origin: '*'
}));

// Page routes
app.get('/', (_, res: any) => { res.send(getWebContent(Routes.Index)) })
app.get('/settings', (_, res: any) => { res.send(getWebContent(Routes.Settings)) })
app.get('/login', (_, res: any) => { res.send(getWebContent(Routes.Login)) })
app.get('/login_qr', (_, res: any) => { res.send(getWebContent(Routes.LoginQR)) })
app.get('/message/:message', (_, res: any) => { res.send(getWebContent(Routes.Message)) })
app.get('/grade/:grade', (_, res: any) => { res.send(getWebContent(Routes.Grade)) })
app.get('/homework-info/:homework', (_, res: any) => { res.send(getWebContent(Routes.HomeworkInfo)) })
app.get('/subject/:id/:order', (_, res: any) => { res.send(getWebContent(Routes.Subject)) })
app.get('/grades', (_, res: any) => { res.send(getWebContent(Routes.Grades)) })
app.get('/final-grades', (_, res: any) => { res.send(getWebContent(Routes.GradesFinal)) })
app.get('/calendar', (_, res: any) => { res.send(getWebContent(Routes.Calendar)) })
app.get('/messages', (_, res: any) => { res.send(getWebContent(Routes.Messages)) })
app.get('/grades-subject', (_, res: any) => { res.send(getWebContent(Routes.Blank)) })
app.get('/homework', (_, res: any) => { res.send(getWebContent(Routes.Blank)) })
app.get('/behavior', (_, res: any) => { res.send(getWebContent(Routes.Behavior)) })
app.get('/behavior-info/:behavior', (_, res: any) => { res.send(getWebContent(Routes.BehaviorInfo)) })

// Static files
app.use('/styles', express.static(path.join(__dirname, "../web/styles")))
app.use('/scripts', express.static(path.join(__dirname, "../web/scripts")))
app.use('/libraries', express.static(path.join(__dirname, "../web/libraries")))
app.use('/images', express.static(path.join(__dirname, "../web/images")))
app.use('/manifest.json', express.static(path.join(__dirname, "../web/manifest.json")))
app.use('/sw.js', express.static(path.join(__dirname, "../web/sw.js")))

try {
  loadSignalHandling()
} catch(err) {
  infoLogger.error(`Failed to load signal handling`)
}

try {
  loadAPIEndpointsV1(app)
  loadAPIEndpointsFakeV1(app)
} catch(err) {
  infoLogger.error(`Failed to load endpoints: ${err}`)
}

parseConfig();

// 404
app.all('*', (_, res: any) => { res.status(404).send(getWebContent(Routes.NotFound)) })

try {
  checkConfig();

  server = app.listen(State.serverPort, State.serverHost, async () => {
    await configureServer();
    watchConfig();

    infoLogger.info("REŠOL: Setup done.")

    console.log();

    infoLogger.info(`REŠOL: Server successfully started at http://${State.serverHost}:${State.serverPort}!`)
  })
} catch(err) {
  infoLogger.error(`REŠOL: Failed to start the server: ${err}`)
}
