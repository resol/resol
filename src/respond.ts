import fs from "fs"

export enum Routes {
  Index = "web/index.html",
  Login = "web/login/login.html",
  LoginQR = "web/login/login_qr.html",
  Message = "web/pages/message.html",
  Messages = "web/pages/messages.html",
  Grade = "web/pages/grade.html",
  Grades = "web/pages/grades.html",
  GradesFinal = "web/pages/final-grades.html",
  HomeworkInfo = "web/pages/homework-info.html",
  Subject = "web/pages/subject-info.html",
  Settings = "web/pages/settings.html",
  Calendar = "web/pages/calendar.html",
  Behavior = "web/pages/behavior.html",
  BehaviorInfo = "web/pages/behavior-info.html",
  NotFound = "web/404.html",
  Blank = "web/pages/blank.html",
}

export function getWebContent(route: Routes) {
  try {
    return fs.readFileSync(route, "utf-8")
  } catch (err) {
    console.error("The file that would be returned was not found")
  }
}
