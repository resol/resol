import { Request, Response } from "express";
import { State } from "../../..";
import os from "os";
import si from "systeminformation"
import { getBasicAuth } from "../createRequest";
import { spawn } from "node:child_process";
import fs from "fs";
import { setTimeout } from 'timers/promises'
import { redisClient, validateRedis } from "../../../cache";

function formatTime(seconds: number) {
  var levels = [
    [Math.floor(seconds / 31536000), 'years'],
    [Math.floor((seconds % 31536000) / 86400), 'days'],
    [Math.floor(((seconds % 31536000) % 86400) / 3600), 'hours'],
    [Math.floor((((seconds % 31536000) % 86400) % 3600) / 60), 'minutes'],
    [Math.round((((seconds % 31536000) % 86400) % 3600) % 60), 'seconds'],
  ];
  var returntext = '';

  for (var i = 0, max = levels.length; i < max; i++) {
    if ( levels[i][0] === 0 ) continue;
    returntext += ' ' + levels[i][0] + ' ' + (levels[i][0] === 1 ? (levels[i][1] as string).substr(0, (levels[i][1] as string).length-1): levels[i][1]);
  };

  return returntext.trim();
}

interface userInfo {
  username: string,
  password: string,
  solUrl: string
}

export function isAdmin(currentUser: userInfo): boolean {
  let admin = false;

  for (let account in State.config.admin) {
    const currentAccount = State.config.admin[account]
    
    if (currentAccount != null) {
      const usernameSame = currentAccount.username.toLowerCase() == currentUser.username 
      const passwordSame = currentAccount.password == currentUser.password
      const solUrlSame = currentAccount.solUrl == currentUser.solUrl 

      if (currentAccount != null && usernameSame && passwordSame && solUrlSame) admin = true
    }
  }

  return admin 
}

export async function getSSEAdvancedServerDetails() {
  const cpu = await si.currentLoad();
  let serverDetails: {} = {};

  if (State.config.sendOSDetails) {
    serverDetails = {
      osUptime: formatTime(os.uptime()),
    }
  }

  return {
    cpuLoad: cpu.currentLoadUser,
    serverDetails: serverDetails ? serverDetails : null
  }
}

export async function getAdvancedServerDetails() {
  const osInfo = await si.osInfo();
  const cpu = await si.currentLoad();
  let serverDetails: {} = {};

  if (State.config.sendOSDetails) {
    serverDetails = {
      osType: os.type(),
      osKernel: os.release(),
      osUptime: formatTime(os.uptime()),
    }
  }

  return {
    distro: osInfo.distro ? osInfo.distro : "",
    osBuild: osInfo.build ? osInfo.build : "",
    cpuLoad: cpu.currentLoadUser,
    serverDetails: serverDetails ? serverDetails : null 
  }
}

export async function getServerDetails(req: Request, res: Response) {
  const json = require("../../../../package.json")

  let serverDetails: {} = {};
  let userAdmin = false;
  
  if (State.config.sendOSDetails) {
    serverDetails = {
      osType: os.type(),
      osKernel: os.release(),
      osUptime: formatTime(os.uptime()),
    }
  }

  // check for admin account
  if (State.config.admin != null && req.headers.authorization && req.headers.sol_url) {
    let authorization: { username: string, password: string }
    
    try {
      authorization = getBasicAuth(req)!
    } catch(err) {
      return res.status(400).send({
        error: true,
        message: "Invalid Authorization header"
      })
    }

    if (authorization) {
      userAdmin = isAdmin({
        username: authorization.username.toLowerCase(),
        password: authorization.password,
        solUrl: req.headers.sol_url.toString()
      })
    }
  }

  const extraTorInstances = State.config.tor.proxyAmount ? State.config.tor.proxyAmount : 0; 

  const serverInfo = {
    instance: {
      name: State.instanceName,
      description: State.instanceDescription,
      redis: State.config.redis.redis ? true : false,
      extraTorInstances: State.config.tor.disableTor ? null : extraTorInstances,
      serverDetails: serverDetails ? serverDetails : null
    },
    softwareDescription: json.description,
    version: json.version,
    homepage: json.repository.url,
    userAdmin: userAdmin,
  }

  return serverInfo
}

const youAreNotAdmin = {
  error: true,
  message: "Nejste administrátor"
}

export async function updateServer(req: Request, res: Response) {
  if (!fs.existsSync(".git")) return res.status(400).send({
    error: true,
    message: "Not in Git directory"
  })

  if (State.config.admin != null && req.headers.authorization && req.headers.sol_url) {
    let userAdmin: boolean;
    let authorization: { username: string, password: string }
    
    try {
      authorization = getBasicAuth(req)!
    } catch(err) {
      return res.status(400).send({
        error: true,
        message: "Invalid Authorization header"
      })
    }

    if (authorization) {
      userAdmin = isAdmin({
        username: authorization.username.toLowerCase(),
        password: authorization.password,
        solUrl: req.headers.sol_url.toString()
      })
    } else {
      return res.status(401).send(youAreNotAdmin)
    }

    if (userAdmin) {
      const gitPull = spawn("git", [ "pull" ]);
      let gitPullOutput = "";

      gitPull.on("data", (data) => {
        gitPullOutput = gitPullOutput.concat(data)
      })

      gitPull.stdout.on('data', (data) => {
        gitPullOutput = gitPullOutput.concat(data.toString())
      })
      
      gitPull.stderr.on('data', (data) => {
        gitPullOutput = gitPullOutput.concat(data.toString())
      })

      gitPull.on("exit", code => {
        let message = "";
        let error: boolean = code !== 0;

        if (gitPullOutput.trim() === "Already up to date.") {
          message = "reŠOL už byl na nejnovějších změnách"
          error = true
        } else if (code === 0) {
          message = "reŠOL byl úspěšně aktualizován"
        } else {
          message = `Nepodařilo se reŠOL aktualizovat: ${gitPullOutput.trim()}`
        }

        return res.send({
          error: error,
          message: message,
          code: code,
          output: gitPullOutput.trim(),
        })
      })
    } else {
      return res.status(401).send(youAreNotAdmin)
    }
  } else {
    return res.status(401).send(youAreNotAdmin)
  }
}

async function systemdRestart() {
  await setTimeout(2000)
  spawn("sudo", [ "systemctl", "restart", "resol" ]);
}

export async function restartServer(req: Request, res: Response) {
  if (State.config.admin != null && req.headers.authorization && req.headers.sol_url) {
    let userAdmin: boolean;
    let authorization: { username: string, password: string }
    
    try {
      authorization = getBasicAuth(req)!
    } catch(err) {
      return res.status(400).send({
        error: true,
        message: "Invalid Authorization header"
      })
    }

    if (authorization) {
      userAdmin = isAdmin({
        username: authorization.username.toLowerCase(),
        password: authorization.password,
        solUrl: req.headers.sol_url.toString()
      })
    } else {
      return res.status(401).send(youAreNotAdmin)
    }

    if (userAdmin) {
      if (State.config.adminCanRestart != true) {
        return res.status(400).send({
          error: true,
          message: "Tato reŠOL instance nepodporuje restartování reŠOL serveru"
        })
      }

      systemdRestart()

      return res.send({
        error: false,
        message: "Proces restartu začal"
      })
    } else {
      return res.status(401).send(youAreNotAdmin)
    }
  } else {
    return res.status(401).send(youAreNotAdmin)
  }
}

export async function cleanRedisAll(req: Request, res: Response) {
  if (State.config.admin != null && req.headers.authorization && req.headers.sol_url) {
    let userAdmin: boolean;
    let authorization: { username: string, password: string }
    
    try {
      authorization = getBasicAuth(req)!
    } catch(err) {
      return res.status(400).send({
        error: true,
        message: "Invalid Authorization header"
      })
    }

    if (authorization) {
      userAdmin = isAdmin({
        username: authorization.username.toLowerCase(),
        password: authorization.password,
        solUrl: req.headers.sol_url.toString()
      })
    } else {
      return res.status(401).send(youAreNotAdmin)
    }

    if (userAdmin) {
      if (!validateRedis()) return res.status(400).send({
        error: true,
        specific: "Tato reŠOL instance nepodporuje Redis caching"
      });

      await redisClient.sendCommand(["FLUSHALL"])

      return res.send({
        error: false,
        message: "Cache pro všechny byla smazána."
      })
    } else {
      return res.status(401).send(youAreNotAdmin)
    }
  } else {
    return res.status(401).send(youAreNotAdmin)
  }

}
