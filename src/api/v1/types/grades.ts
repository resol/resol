import axios, { AxiosError, AxiosResponse } from "axios";
import { getAccessToken, requestAllInformation } from "../createRequest";
import { requestType } from "../requests";
import { getCache, setCache } from "../../../cache";
import { State } from "../../..";

export async function grades(req: requestAllInformation) {
  if (!req.request.query.studentId) {
    return req.response.status(400).send({
      error: true,
      message: "No studentId query was found in the URL query"
    })
  }

  const pageSize = req.request.query.pageSize ? req.request.query.pageSize.toString() : "30";
  const pageNumber = req.request.query.pageNumber ? req.request.query.pageNumber.toString() : "1";

  // available: `all`, `unsigned`
  const signingFilter = req.request.query.signingFilter ? req.request.query.signingFilter.toString() : "all";
  
  if (signingFilter != "all" && signingFilter != "unsigned") {
    return req.response.status(400).send({
      error: true,
      message: "Invalid signingFilter (available: `all`, `unsigned`)"
    })
  }

  const gradesCache = getCache({
    cacheQuery: `grades-${req.request.query.studentId}-${pageSize}-${pageNumber}-${signingFilter}`,
  }, req.auth)

  if (await gradesCache) return req.response.send(await gradesCache)
  let token = await getAccessToken(req.auth, req.request, req.response);
  if(!token) return

  let gradesQuery = new URLSearchParams({
    "SigningFilter": signingFilter,
    "Pagination.PageNumber": pageNumber,
    "Pagination.PageSize": pageSize,
  })

  let response: AxiosResponse;
  try {
    response = await axios.request({
      url: `${req.auth.solUrl}/${requestType.Grades}/${req.request.query.studentId}/marks/list?${gradesQuery}`,
      method: "GET",
      httpsAgent: req.torProxyAgent,
      httpAgent: req.torProxyAgent,
      headers: {
        Authorization: `Bearer ${await token}`
      }
    })
  } catch(err) {
    return req.response.status(400).send({
      error: true,
      message: "Špatný request",
      specific: (err as AxiosError).message
    })
  }

  if (response.data) {
    setCache(response.data, {
      cacheQuery: `grades-${req.request.query.studentId}-${pageSize}-${pageNumber}-${signingFilter}`,
      expiration: State.config.caching.grades ||  60 * 10, // 10 minutes 
    }, req.auth)
    return req.response.send(response.data)
  } else {
    return req.response.status(400).send({
      error: true,
      message: "Žádné data",
    })
  }
}
