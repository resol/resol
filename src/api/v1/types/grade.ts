import axios, { AxiosError, AxiosResponse } from "axios";
import { getAccessToken, requestAllInformation } from "../createRequest";
import { requestType } from "../requests";
import { getCache, setCache } from "../../../cache";
import { State } from "../../..";

export async function grade(req: requestAllInformation) {
    if (!req.request.query.studentId) {
      return req.response.status(400).send({
        error: true,
        message: "No studentId query was found in the URL query"
      })
    }
    const gradeCache = getCache({
        cacheQuery: `grade-${req.request.query.gradeId}-${req.request.query.studentId}`,
      }, req.auth)

    if (await gradeCache) return req.response.send(await gradeCache)
    
    let token = await getAccessToken(req.auth, req.request, req.response);
    if(!token) return

    let response: AxiosResponse;

    try {
      response = await axios.request({
        url: `${req.auth.solUrl}/${requestType.Grade}/${req.request.query.gradeId}?${req.request.query.studentId}`,
        method: "GET",
        httpsAgent: req.torProxyAgent,
        httpAgent: req.torProxyAgent,
        headers: {
          Authorization: `Bearer ${await token}`
        }
      })
    } catch(err) {
      return req.response.status(400).send({
        error: true,
        message: "Špatný request",
        specific: (err as AxiosError).message
      })
    }
  
    if (response.data) {
      setCache(response.data, {
        cacheQuery: `grade-${req.request.query.gradeId}-${req.request.query.studentId}`,
        expiration: State.config.caching.grade ||  60 * 10, // 10 minutes 
      }, req.auth)
      return req.response.send(response.data)
    } else {
      return req.response.status(400).send({
        error: true,
        message: "Žádné data",
      })
    }
}
