import { Application, Request, Response } from "express"
import { createRequest, getBasicAuth } from "./createRequest"
import { computeSHA256, redisClient, validateRedis } from "../../cache"
import { State } from "../.."
import { cleanRedisAll, getAdvancedServerDetails, getSSEAdvancedServerDetails, getServerDetails, restartServer, updateServer } from "./types/info";

export enum requestType {
  Login = "solapi/api/connect/token",
  UserInfo = "solapi/api/v1/user",
  Grades = "solapi/api/v1/students", // this is not the full url
  Grade = "solapi/api/v1/student/marks",
  Messages = "solapi/api/v1/messages/received",
  MessageRead = "solapi/api/v1/messages",
  MessageDelete = "solapi/api/v1/messages",
  MessagesSent = "solapi/api/v1/messages/sent",
  Homework = "solapi/api/v1/students", // this is not the full url
  Attachment = "solapi/api/v1/student/homeworks/assignment/attachments",
  AttachmentMessage = "solapi/api/v1/messages",
  TimeTable = "solapi/api/v1/timeTable",
  Recipients = "solapi/api/v1/messages/recipients",
  Behavior = "solapi/api/v1/students"
}

export function loadAPIEndpointsV1(app: Application) {
  app.get('/api/v1/verify', (req: Request, res: Response) => {
    createRequest(req, res, { tag: "login" })
  })

  app.get('/api/v1/user', (req: Request, res: Response) => {
    createRequest(req, res, { tag: "userinfo" })
  })

  app.get('/api/v1/grades', (req: Request, res: Response) => {
    createRequest(req, res, { tag: "grades" })
  })

  app.get('/api/v1/grade', (req: Request, res: Response) => {
    createRequest(req, res, { tag: "grade" })
  })

  app.get('/api/v1/gradesFinal', (req: Request, res: Response) => {
    createRequest(req, res, { tag: "gradesFinal" })
  })

  app.get("/api/v1/messages", (req: Request, res: Response) => {
    createRequest(req, res, { tag: "messages" })
  })

  app.put("/api/v1/messageRead", (req: Request, res: Response) => {
    createRequest(req, res, { tag: "messageRead" })
  })

  app.delete("/api/v1/messages", (req: Request, res: Response) => {
    createRequest(req, res, { tag: "messageDelete" })
  })
  
  app.get("/api/v1/homework", (req: Request, res: Response) => {
    createRequest(req, res, { tag: "homework" })
  })

  app.get("/api/v1/behavior", (req: Request, res: Response) => {
    createRequest(req, res, { tag: "behavior" })
  })

  app.get("/api/v1/timetable", (req: Request, res: Response) => {
    createRequest(req, res, { tag: "timetable" })
  })

  app.get("/api/v1/recipients", (req: Request, res: Response) => {
    createRequest(req, res, { tag: "recipients" })
  })

  app.get("/api/v1/attachment/:attachment", (req: Request, res: Response) => {
    createRequest(req, res, { tag: "attachment" })
  })

  app.get("/api/v1/messageAttachment/:attachment", (req: Request, res: Response) => {
    createRequest(req, res, { tag: "attachmentmessage" })
  })

  // resol info
  app.get("/api/v1/resol/info", async (req: Request, res: Response) => {
    const serverDetails = await getServerDetails(req, res);

    if (res.writableEnded) return

    res.send(serverDetails)
  })
  
  // resol info with sse
  app.get("/api/v1/sse/resol/info", async (req: Request, res: Response) => {
    res.writeHead(200, {
      "Connection": "keep-alive",
      "Cache-Control": "no-cache",
      "Content-Type": "text/event-stream",
    });

    const interval = setInterval(async () => {
      const serverDetails = await getSSEAdvancedServerDetails();
      const chunk = JSON.stringify({chunk: serverDetails});
      res.write(`data: ${chunk}\n\n`)
    }, State.config.instanceInfoSSEInterval ? State.config.instanceInfoSSEInterval : 2000)

    res.on("close", () => {
      clearInterval(interval);
      res.end();
    });
  })

  // resol git update 
  app.get("/api/v1/resol/update", async (req: Request, res: Response) => {
    await updateServer(req, res)
  })

  // resol systemd restart 
  app.get("/api/v1/resol/restart", async (req: Request, res: Response) => {
    await restartServer(req, res)
  })

  app.get("/api/v1/resol/advancedInfo", async (req: Request, res: Response) => {
    if (!State.config.sendOSDetails) {
      return res.status(400).send({
        error: true,
        message: "This instance doesn't have enabled OS details"
      })
    }

    res.send(await getAdvancedServerDetails())
  })

  // admin: delete cache for all
  app.get("/api/v1/cache/cleanAll", async (req: Request, res: Response) => {
    await cleanRedisAll(req, res)
  })

  // delete cache
  app.get("/api/v1/cache/clean", async (req: Request, res: Response) => {
    if (!validateRedis()) return res.status(400).send({
      error: true,
      specific: "Tato reŠOL instance nepodporuje Redis caching"
    });

    let auth;

    const authBasic = getBasicAuth(req)
    if (authBasic) {
      auth = authBasic
    } else {
      return res.status(400).send({
        error: true,
        message: "No Authorization header"
      })
    }

    if(!req.headers["sol-url"]) {
      return res.status(400).send({
        error: true,
        message: "No sol-url header"
      })
    }

    let authObj: any = {
      username: auth.username,
      password: auth.password,
      solUrl: req.headers["sol-url"],
    }
    const username = computeSHA256(authObj.username.toLowerCase())
    const password = computeSHA256(authObj.password)
    const solUrl = computeSHA256(authObj.solUrl)

    const accessHash = computeSHA256("access")

    let cursor = "0"
    let key: string;

    const reply = await redisClient.scan(cursor, { MATCH: `${username}-${password}-${solUrl}*`, COUNT: 1000 });
    
    if (reply.keys.length == 0 || (reply.keys.length == 1 && reply.keys[0] == `${username}-${password}-${solUrl}-${accessHash}`)) {
      return res.status(400).send({
        error: true,
        specific: "Tento uživatel již nemá žádné data v cachi"
      })
    }

    for (key of reply.keys) {
      cursor = reply.cursor;
      
      // don't delete access token in cache
      if (key == `${username}-${password}-${solUrl}-${accessHash}`) continue

      await redisClient.del(key);
    }

    res.send({
      error: false,
      message: `🚀 Cache se úspěšně vymazala (${reply.keys.length})`
    })
  })
}
