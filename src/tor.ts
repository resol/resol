import { spawn } from "node:child_process";
import { SocksProxyAgent } from "socks-proxy-agent";
import { State, infoLogger } from ".";
import fs from "fs";
import path from "path";
import axios, { AxiosError } from "axios";
import { exit } from "process";

let someCreated = false;

function getRandomInt(max: number) {
  return Math.floor(Math.random() * max);
}

export async function checkTorMasterProxy() {
  const torHost: string = State.config.tor.host || "127.0.0.1"
  const torPort: number = State.config.tor.port || 9050

  const torMasterProxy = new SocksProxyAgent(`socks://${torHost}:${torPort}`)
  const pingUrl = State.config.tor.pingUrl || `https://gnu.org`

  infoLogger.info(`TOR: Pinging ${pingUrl}..`)

  try {
    await axios.request({
      url: pingUrl,
      method: "GET",
      httpsAgent: torMasterProxy,
      httpAgent: torMasterProxy,
    })
    
    infoLogger.info(`TOR: Ping was successful`)
  } catch(err) {
    if((err as AxiosError).message.includes("connect ECONNREFUSED")) {
      infoLogger.error("TOR: The master Tor proxy refused to connect. Please check if your Tor proxy is running or if there are set correct options in `config.yaml`.")
      
      exit(1);
    }

    infoLogger.warn(`TOR: The request to ${pingUrl} failed: ${(err as AxiosError).message}`)
  }
}

export function getProxy() {
  const torHost: string = State.config.tor.host || "127.0.0.1"
  const torPort: number = State.config.tor.port || 9050

  if (State.config.tor.proxyAmount > 0) {
    const random = 29001 + getRandomInt(Number(State.config.tor.proxyAmount) - 1);
    return new SocksProxyAgent(`socks://${torHost}:${random}`)
  } else {
    return new SocksProxyAgent(`socks://${torHost}:${torPort}`)
  }
}

function createTorConfig(index: number) {
  const stream = fs.createWriteStream(path.join(__dirname, `../tor-group/torrc.${index}`))
  stream.once("open", () => {
    stream.write(`SocksPort ${29000 + index}\n`)
    stream.write(`DataDirectory tor-group-${index}`)
    stream.end()
  })
}

async function checkTorRC(index: number) {
  if (!fs.existsSync(path.join(__dirname, `../tor-group/torrc.${index}`))) {
    infoLogger.info(`Creating config for Tor proxy #${index}`);
    createTorConfig(index);
    someCreated = true;
  }
}

export async function startTor(amount: number) { 
  if (!fs.existsSync(path.join(__dirname, `../tor-group`))) {
    infoLogger.info(`Creating directory for Tor proxies`);
    fs.mkdirSync(path.join(__dirname, `../tor-group`))
  } 

  for (let i = 1; i < amount; i++) {
    await checkTorRC(i)
    await startTorService(i)
  }

  if (someCreated) {
    infoLogger.info("TOR: It can take a few seconds or minutes to start all of the Tor proxies")
  }
}

async function startTorService(index: Number) {
  // infoLogger.info(`TOR #${index}: Spawning Tor instance`)
  const tor = spawn("tor", ["-f", `tor-group/torrc.${index}`]);

  tor.stdout.on('data', function(data) {
    if (data.includes("Bootstrapped 100%")) {
      infoLogger.info(`TOR #${index}: successfully started`)
    }
  });

  tor.stderr.setEncoding('utf8');

  tor.stderr.on('data', function(data) {
    infoLogger.error(`TOR #${index}: ${data}`);
  });
}
