import { universalAuth } from "./api/v1/createRequest";
import { computeSHA256, redisClient, validateRedis } from "./cache";

export async function createLock(auth: universalAuth) {
  if(!validateRedis()) return;

  const username = computeSHA256(auth.username.toLowerCase())
  const password = computeSHA256(auth.password)
  const solUrl = computeSHA256(auth.solUrl!)

  await redisClient.set(
    `${username}-${password}-${solUrl}-lock`,
    "true",
    { EX: 4 } // 4 seconds lock expiration
  )
}

export async function checkLock(auth: universalAuth) {
  if(!validateRedis()) return;

  const username = computeSHA256(auth.username.toLowerCase())
  const password = computeSHA256(auth.password)
  const solUrl = computeSHA256(auth.solUrl!)

  while (true) {
    const lock = await redisClient.get(
      `${username}-${password}-${solUrl}-lock`,
    )

    // if lock is not found
    if (!lock) break
    
    // wait 50 milliseconds
    await new Promise(r => setTimeout(r, 50));
  }
}

export async function deleteLock(auth: universalAuth) {
  if(!validateRedis()) return;

  const username = computeSHA256(auth.username.toLowerCase())
  const password = computeSHA256(auth.password)
  const solUrl = computeSHA256(auth.solUrl!)

  redisClient.del(
    `${username}-${password}-${solUrl}-lock`,
  )
}
