import { server, infoLogger } from "."

export function loadSignalHandling() {
  const shutDownPolitely = () => {
    infoLogger.info("Shutting down the server politely..")

    server.close(() => {
      infoLogger.info("Successfully shut down the server!")
      process.exit(0)
    }) 
  }

  process.on("SIGTERM", shutDownPolitely)
}
