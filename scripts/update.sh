#!/bin/bash

echo "===== Vypínám reŠOL service ====="
sudo systemctl stop resol

echo "===== Kopíruji soubory ====="

mkdir -p /tmp/resol/web
cp -r ./web/* /tmp/resol/web &> /dev/null
cp -r ./tor-group /tmp/resol/tor-group
cp ./target/release/re-sol /tmp/resol/re-sol

chmod 777 /tmp/resol/re-sol || exit 1

sudo su resol -c "cp -r /tmp/resol/* ~/"

rm -rf /tmp/resol

echo "===== Zapínám reŠOL service ====="
sudo systemctl start resol

echo "===== HOTOVO ====="