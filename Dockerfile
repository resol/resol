FROM node:20

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . . 

RUN npx tsc -p ./web/scripts

EXPOSE 3009

ENTRYPOINT [ "./scripts/docker-entrypoint.sh" ]
