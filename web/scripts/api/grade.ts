const get_grade = async () => {
  
    let user = localStorage.getItem("username")
    let pass = localStorage.getItem("password")
    let sol_url: string = (localStorage.getItem("sol_url") ? localStorage.getItem("sol_url")! : "https://aplikace.skolaonline.cz");
  
    const gradeId = window.location.pathname.split("/").pop();
    const studentId = localStorage.getItem("selected_zak")! || localStorage.getItem("osoba_id")!;
    let url = `/api/v1/grade?gradeId=${gradeId}&studentId=${studentId}`;
  
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", url, true ); // false for synchronous request
    xmlHttp.setRequestHeader("Authorization", "Basic " + btoa(user + ":" + pass))
    xmlHttp.setRequestHeader("sol-url", sol_url);
  
    xmlHttp.send();
    xmlHttp.onload = () => {
      handle_grade_data(xmlHttp.responseText)
    }
  }

const handle_grade_data = (xmlHttp: string) => {
  let json = JSON.parse(xmlHttp);
  document.title = json["theme"] + " · reŠOL";

  document.getElementById("page-info-container")!.style.display = "block";
  document.getElementById("loading-page-info-data")!.style.display = "none";

  document.getElementById("grade-title")!.textContent = json["theme"];
  document.getElementById("grade-subject")!.textContent = json["subjectName"];
  document.getElementById("grade-result")!.textContent = json["markText"];
  document.getElementById("grade-result")!.style.color = assign_grade_color(Number(json["markText"]));
  document.getElementById("grade-weight")!.textContent = json["weight"];
  document.getElementById("grade-average")!.textContent = json["classAverage"];
  document.getElementById("grade-rank")!.textContent = json["classRankText"];
  document.getElementById("grade-teacher")!.textContent = json["teacherDisplayName"];
  if (convert_date(json["markDate"]) != convert_date(json["editDate"])) {
    document.getElementById("grade-date")!.textContent = convert_date(json["markDate"]) + ", upraveno " + convert_date(json["editDate"]);
  } else {
    document.getElementById("grade-date")!.textContent = convert_date(json["markDate"]);
  }
  document.getElementById("grade-hour")!.textContent = json["lessonId"];
  if (json["comment"]) document.getElementById("grade-note")!.textContent = json["comment"];
  document.getElementById("grade-rating")!.textContent = json["typeNote"];
}
