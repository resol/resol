const hamburger = document.querySelector(".hamburger")!;
const header = document.querySelector(".header")!;
const body = document.querySelector("body")!;

hamburger.addEventListener("click", () => {
    hamburger.classList.toggle("active");
    header.classList.toggle("active");
    body.classList.toggle("active");
})

const navMenu = document.querySelector(".nav-menu")!;
const widthLimit = 650;

// check if window width is smaller than limit, if yes then add EventListener to hide navmenu on click
// this doesn't think of resizing
const check_window_width = () => {
    let windowWidth = window.innerWidth;
    if (windowWidth < widthLimit) {
        navMenu.addEventListener("click", () => {
            hamburger.classList.toggle("active");
            header.classList.toggle("active");
            body.classList.toggle("active");
        })
    }
}

check_window_width();
