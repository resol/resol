---
### SERVER

server:
  # ip address where your reŠOL instance is hosted on (something like 127.0.0.1 or 0.0.0.0) - string
  host: "0.0.0.0"

  # port of your reŠOL instance - int
  port: 3009

### GENERAL

# Name of the instance (`false` for default) - string
instanceName: "reŠOL"

# Description of the instance (`false` for default) - bool
instanceDescription: false

# Enable TypeScript auto-compilation for web scripts - bool
compileWebTs: true

# Send your OS details in /api/v1/resol/info - bool
sendOSDetails: false

# Enable restarting reŠOL systemd service using an admin account (requires no sudo password) - bool
adminCanRestart: false

# Interval of sending advanced server information (defined in ms, 0 for default) - int
instanceInfoSSEInterval: 2000 


### REDIS
# Redis is really useful for user data caching and dramatically improves overall performance.

redis:
  # if you want to connect your reŠOL instance to Redis, change this to true - bool
  redis: false 

  # your Redis host IP address (something like 127.0.0.1 or 0.0.0.0) - string
  host: "127.0.0.1"

  # change this if your Redis database is running on a different port - int
  port: 6379

  # your Redis database user (keep this `false` if there's no username set in Redis) - string 
  user: false 

  # your Redis database password (keep this `false` if there's no password set in Redis) - string
  password: false


### CACHING SETTINGS (works only with Redis enabled, defined in seconds, 0 for no expiration) - int

caching:
  user: 172800 # 2 days
  grades: 600 # 10 minutes
  gradesFinal: 600 # 10 minutes
  homework: 600 # 10 minutes
  messages: 600 # 10 minutes
  recipients: 172800 # 2 days
  timetable: 600 # 10 minutes
  behavior: 600 # 10 minutes


### TOR PROXY GROUP
# Tor proxy group is good to have enabled if you are running in production.
# TLDR: it prevents from ŠOL rate limiting, but it tends to increase server's memory usage

tor:
  # Tor proxy host IP address - string
  host: "127.0.0.1"

  # Master Tor proxy port - int
  port: 9050

  # Amount of extra Tor proxies (recommended amount for production is around 50, 0 for no extra proxies, not effective when higher than 6000) - int
  proxyAmount: 10 

  # The URL to ping to check if Tor proxy is working right (`false` to disable) - string
  pingUrl: "https://gnu.org"

  # Fully disable Tor for requests
  disableTor: true 


### ADMIN ACCOUNTS
# Admin accounts can pull the latest code changes live

# admin:
#   main:
#     username: "1.novakjan"
#     password: "jablkohruskaorangutan"
#     solUrl: "https://aplikace.skolaonline.cz"
...
